Yii2 DHL api component
======================
Connect you Yii2 application with the DHL parcel tracking API. This project was created to work with API version 1.1.0.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dbx12/yii2-dhl "*"
```

or add

```
"dbx12/yii2-dhl": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, configure it as component in your config files. You need to register
[here](https://developer.dhl.com/) on the DHL Developer Portal to get an API key for the _Shipment Tracking - Unified_
API. Put the _consumer key_ in your config file as seen below.

```php
return [
    'components' => [
        'dhl' => [
            'class' => \dbx12\dhl_component\DhlComponent::class,
            'consumerKey' => 'consumer-key-here'
        ],
    ],
];
```

You can than use it as component, for example, if you want to find out about the parcel with the tracking number
`00123456789123456789`, you would do this:

```php
$shipment = Yii::$app->dhl->getShipment('00123456789123456789');
```

The return value is basically a `\yii\base\Model` object, the structure is mostly oriented on the documentation on the
documented schema [here](https://developer.dhl.com/api-reference/shipment-tracking#reference-docs-section%schemas). Some
differences exist due to the inner workings of Yii models.
