<?php


namespace dbx12\dhl_component\tests;

use Codeception\Test\Unit;
use ReflectionClass;
use ReflectionException;

abstract class UnitTestCase extends Unit
{

    /**
     * Invokes a inaccessible method
     *
     * @param object|string $object The object to invoke the method on or the class name in case of a static method
     * @param string $methodName    Name of method to invoke
     * @param array $args           Parameters to be passed to the method, as an array.
     * @param bool $revoke          whether to make method inaccessible after execution
     * @return mixed result of the invoked method
     * @throws ReflectionException If the method does not exist
     */
    protected function invokeMethod($object, string $methodName, array $args = [], bool $revoke = true)
    {
        $reflection = new ReflectionClass($object);
        $reflectionMethod = $reflection->getMethod($methodName);
        $reflectionMethod->setAccessible(true);

        if ($reflectionMethod->isStatic()) {
            $result = $reflectionMethod->invokeArgs(null, $args);
        } else {
            $result = $reflectionMethod->invokeArgs($object, $args);
        }

        if ($revoke) {
            $reflectionMethod->setAccessible(false);
        }
        return $result;
    }

    /**
     * Sets an inaccessible object property to a designated value
     *
     * @param object $object Object to set the property on
     * @param string $propertyName Name of the property
     * @param mixed $value new value of the property
     * @param bool $revoke whether to make property inaccessible after setting
     * @throws ReflectionException if there is no property with $propertyName
     */
    protected function setInaccessibleProperty(object $object, string $propertyName, $value, $revoke = true): void
    {
        /** @noinspection PhpUnhandledExceptionInspection class must exist if there is an object of it */
        $class = new ReflectionClass($object);
        while (!$class->hasProperty($propertyName)) {
            $class = $class->getParentClass();
        }
        $property = $class->getProperty($propertyName);
        $property->setAccessible(true);
        $property->setValue($object, $value);
        if ($revoke) {
            $property->setAccessible(false);
        }
    }

    /**
     * Gets an inaccessible object property
     *
     * @param object $object       Object to get the property from
     * @param string $propertyName Name of the property
     * @param bool $revoke         whether to make property inaccessible after getting
     * @return mixed value of the property
     * @throws ReflectionException if there is no property with $propertyName
     */
    protected function getInaccessibleProperty(object $object, string $propertyName, $revoke = true)
    {
        /** @noinspection PhpUnhandledExceptionInspection class must exist if there is an object of it */
        $class = new ReflectionClass($object);
        while (!$class->hasProperty($propertyName)) {
            $class = $class->getParentClass();
        }
        $property = $class->getProperty($propertyName);
        $property->setAccessible(true);
        $result = $property->getValue($object);
        if ($revoke) {
            $property->setAccessible(false);
        }
        return $result;
    }
}
