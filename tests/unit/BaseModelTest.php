<?php

namespace dbx12\dhl_component\tests\unit;

use dbx12\dhl_component\models\Address;
use dbx12\dhl_component\models\BaseModel;
use dbx12\dhl_component\models\Person;
use dbx12\dhl_component\tests\UnitTestCase;

class BaseModelTest extends UnitTestCase
{
    public function testConstructor()
    {
        $config = [
            'scalar'  => 14,
            'address' => [
                'countryCode'   => 'US',
                'postalCode'    => '80085',
                'streetAddress' => '742 Evergreen Terrace',
            ],
            'persons' => [
                [
                    'familyName' => 'Simpson',
                    'name'       => 'Bart',
                ], [
                    'familyName' => 'Simpson',
                    'name'       => 'Lisa',
                ],
            ],
        ];
        $object = new TestModel($config);
        self::assertEquals($config['scalar'],$object->scalar);
        self::assertEquals($config['address']['countryCode'], $object->address->countryCode);
        self::assertEquals($config['address']['postalCode'], $object->address->postalCode);
        self::assertEquals($config['address']['streetAddress'], $object->address->streetAddress);

        self::assertCount(count($config['persons']), $object->persons);
        self::assertEquals($config['persons'][0]['familyName'], $object->persons[0]->familyName);
        self::assertEquals($config['persons'][1]['familyName'], $object->persons[1]->familyName);
        self::assertEquals($config['persons'][0]['name'], $object->persons[0]->name);
        self::assertEquals($config['persons'][1]['name'], $object->persons[1]->name);

        self::assertNull($object->notSet);

    }
}

class TestModel extends BaseModel
{
    /** @var int */
    public $scalar;
    /** @var Address */
    public $address;
    /** @var Person[] */
    public $persons;
    /** @var Address */
    public $notSet;

    protected $classMap = [
        'address' => Address::class,
        'persons' => [Person::class],
        'notSet' => Address::class,
    ];

    public function createObjects($config)
    {
        parent::createObjects($config);
    }
}
