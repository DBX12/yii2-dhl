<?php

namespace dbx12\dhl_component\tests\unit;

use dbx12\dhl_component\DhlComponent;
use dbx12\dhl_component\tests\UnitTestCase;

class DhlComponentTest extends UnitTestCase
{

    public function testInit()
    {
        // init is called from constructor
        $component = new DhlComponent(['consumerKey' => 'consumer-key','baseUrl' => 'http://example.org/']);
        $service = $this->getInaccessibleProperty($component,'service');
        self::assertNotNull($service,'Service is created');

    }
}
