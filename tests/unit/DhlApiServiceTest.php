<?php

namespace dbx12\dhl_component\tests\unit;

use Codeception\Test\Unit;
use dbx12\dhl_component\DhlApiService;
use dbx12\dhl_component\ShipmentStatusCode;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use UnitTester;

class DhlApiServiceTest extends Unit
{

    /** @var UnitTester */
    protected $tester;

    /** @var DhlApiService */
    protected $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = new DhlApiService('http://example.org/', 'consumer-key');
    }

    public function testInvalidConsumerKey()
    {
        $mock = new MockHandler([
            new Response(401, [], '{"status":401,"title":"Unauthorized","detail":"Unauthorized for given resource."}'),
        ]);
        $this->service->handlerStack = HandlerStack::create($mock);
        $this->expectExceptionCode(401);
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->service->getShipment('12345678900');
        // no more tests after an exception
    }

    public function testUnknownShipment()
    {
        $mock = new MockHandler([
            new Response(404, [], '{"title":"No result found","status":404,"detail":"No shipment with given tracking number found."}'),
        ]);
        $this->service->handlerStack = HandlerStack::create($mock);
        $shipment = $this->service->getShipment('12345678900');
        self::assertNull($shipment, 'Unknown shipment is returned as null');
    }

    public function testQuotaExceeded()
    {
        $mock = new MockHandler([
            // the body and headers except the status code are unknown and not document by DHL at the moment
            new Response(429),
        ]);
        $this->service->handlerStack = HandlerStack::create($mock);
        $this->expectExceptionCode(429);
        $this->service->getShipment('12345678900');
    }

    public function testSuccess()
    {
        $mock = new MockHandler(
        // tracking data is example data from DHL documentation
            [
                new Response(200, ['Content-Type' => 'application/json'], '{"shipments":[{"serviceUrl":"https://www.dhl.de/de/privatkunden.html?piececode=00340434292135100100&cid=c_dhl_de_352_20205002_151_M040","rerouteUrl":"https://www.dhl.de/de/privatkunden.html?piececode=00340434292135100100&verfuegen_selected_tab=FIRST&cid=c_dhl_de_352_20205001_150_M040","id":"00340434292135100100","service":"parcel-de","origin":{"address":{"countryCode":"DE"}},"destination":{"address":{"countryCode":"DE"}},"status":{"timestamp":"2020-07-01T21:52:00","statusCode":"pre-transit","status":"The instruction data for this shipment have been provided by the sender to DHL electronically","description":"The instruction data for this shipment have been provided by the sender to DHL electronically"},"details":{"totalNumberOfPieces":1,"pieceIds":["340434292135100100"],"weight":{"value":2,"unitText":"kg"},"dimensions":{"width":{"value":0.3,"unitText":"m"},"height":{"value":0.14,"unitText":"m"},"length":{"value":0.38,"unitText":"m"}}},"events":[{"timestamp":"2020-07-01T21:52:00","statusCode":"pre-transit","status":"The instruction data for this shipment have been provided by the sender to DHL electronically","description":"The instruction data for this shipment have been provided by the sender to DHL electronically"},{"timestamp":"2020-06-10T19:01:00","statusCode":"pre-transit","status":"The instruction data for this shipment have been provided by the sender to DHL electronically","description":"The instruction data for this shipment have been provided by the sender to DHL electronically"}]}],"possibleAdditionalShipmentsUrl":["/track/shipments?trackingNumber=00340434292135100100&service=ecommerce","/track/shipments?trackingNumber=00340434292135100100&service=dgf","/track/shipments?trackingNumber=00340434292135100100&service=freight","/track/shipments?trackingNumber=00340434292135100100&service=parcel-nl","/track/shipments?trackingNumber=00340434292135100100&service=parcel-pl"]}'),
            ]
        );
        $this->service->handlerStack = HandlerStack::create($mock);
        $shipment = $this->service->getShipment('00340434292135100100');
        self::assertNotNull($shipment, 'Shipment returned');
        self::assertEquals('00340434292135100100', $shipment->id, 'Shipment id is correct');
        self::assertEquals('parcel-de', $shipment->service, 'Service is correct');
        // test data set does not contain a more concise address
        self::assertEquals('DE', $shipment->origin->address->countryCode, 'Sender country is correct');
        self::assertEquals('DE', $shipment->destination->address->countryCode, 'Receiver country is correct');
        self::assertEquals('2020-07-01T21:52:00', $shipment->status->timestamp, 'Timestamp is correct');
        self::assertEquals(ShipmentStatusCode::PRE_TRANSIT, $shipment->status->statusCode, 'Status code is correct');
        self::assertCount(2, $shipment->events, 'Shipment has two events');
    }

}
