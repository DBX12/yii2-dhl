<?php


namespace dbx12\dhl_component;

use yii\base\Component;

/**
 * Class Dhl
 *
 * @package dbx12\dhl_component\src
 */
class DhlComponent extends Component
{
    /** @var string */
    public $consumerKey;

    /** @var string */
    public $baseUrl = 'https://api-eu.dhl.com/track/';

    /** @var DhlApiService */
    protected $service;

    public function init()
    {
        parent::init();
        $this->service = new DhlApiService($this->baseUrl, $this->consumerKey);
    }

    /**
     * Searches a shipment by tracking number
     *
     * @param string $trackingNumber
     * @return models\Shipment|null null if no shipment was found by this tracking number
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\base\Exception on non-200 responses
     * @codeCoverageIgnore as it only calls another fully tested method
     */
    public function getShipment(string $trackingNumber): ?models\Shipment
    {
        return $this->service->getShipment($trackingNumber);
    }
}
