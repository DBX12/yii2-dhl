<?php


namespace dbx12\dhl_component;


abstract class ShipmentStatusCode
{
    public const PRE_TRANSIT = 'pre-transit';
    public const TRANSIT = 'transit';
    public const DELIVERED = 'delivered';
    public const FAILURE = 'failure';
    public const UNKNOWN = 'unknown';
}
