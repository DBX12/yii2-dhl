<?php


namespace dbx12\dhl_component\models;

/**
 * Class ShipmentEvent
 *
 * @package dbx12\dhl_component\models
 * @property string timestamp
 * @property Place location
 * @property string statusCode
 * @property string status
 * @property string description
 * @property string remark
 * @property string nextSteps
 */
class ShipmentEvent extends BaseModel
{
    /** @var string */
    public $timestamp;
    /** @var Place */
    public $location;
    /** @var string */
    public $statusCode;
    /** @var string */
    public $status;
    /** @var string */
    public $description;
    /** @var string */
    public $remark;
    /** @var string */
    public $nextSteps;

    protected $classMap = [
        'location' => Place::class,
    ];
}
