<?php


namespace dbx12\dhl_component\models;

/**
 * Class Place
 *
 * @package dbx12\dhl_component\models
 * @property Address address
 */
class Place extends BaseModel
{
    /** @var Address */
    public $address;

    protected $classMap = [
        'address' => Address::class,
    ];
}
