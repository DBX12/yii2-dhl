<?php


namespace dbx12\dhl_component\models;

/**
 * Class Dimensions
 *
 * @package dbx12\dhl_component\models
 * @property double width
 * @property double height
 * @property double length
 */
class Dimensions extends BaseModel
{
    /** @var UnitValue */
    public $width;
    /** @var UnitValue */
    public $height;
    /** @var UnitValue */
    public $length;

    protected $classMap = [
        'width'  => UnitValue::class,
        'height' => UnitValue::class,
        'length' => UnitValue::class,
    ];
}
