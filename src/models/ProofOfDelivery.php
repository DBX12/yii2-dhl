<?php


namespace dbx12\dhl_component\models;

/**
 * Class ProofOfDelivery
 *
 * @package dbx12\dhl_component\models
 * @property string timestamp
 * @property string signatureUrl
 * @property string documentUrl
 * @property Person signed
 */
class ProofOfDelivery extends BaseModel
{
    /** @var string */
    public $timestamp;
    /** @var string */
    public $signatureUrl;
    /** @var string */
    public $documentUrl;
    /** @var Person */
    public $signed;

    protected $classMap = [
        'signed' => Person::class,
    ];
}
