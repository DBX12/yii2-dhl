<?php


namespace dbx12\dhl_component\models;

/**
 * Class Person
 *
 * @package dbx12\dhl_component\models
 * @property string familyName
 * @property string givenName
 * @property string name
 */
class Person extends BaseModel
{
    /** @var string */
    public $familyName;
    /** @var string */
    public $givenName;
    /** @var string */
    public $name;
}
