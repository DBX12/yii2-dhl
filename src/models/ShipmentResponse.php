<?php

namespace dbx12\dhl_component\models;

/**
 * Class ShipmentResponse
 *
 * @package dbx12\dhl_component\models
 * @property string url
 * @property string prevUrl
 * @property string nextUrl
 * @property string firstUrl
 * @property string lastUrl
 * @property Shipment[] shipments
 * @property string[] possibleAdditionalShipmentsUrl
 */
class ShipmentResponse extends BaseModel
{
    /** @var string */
    public $url;
    /** @var string */
    public $prevUrl;
    /** @var string */
    public $nextUrl;
    /** @var string */
    public $firstUrl;
    /** @var string */
    public $lastUrl;
    /** @var Shipment[] */
    public $shipments;
    /** @var string[] */
    public $possibleAdditionalShipmentsUrl;

    protected $classMap = [
        'shipments' => [Shipment::class],
    ];

}
