<?php


namespace dbx12\dhl_component\models;

/**
 * Class Address
 *
 * @package dbx12\dhl_component\models
 * @property string countryCode
 * @property string postalCode
 * @property string addressLocality
 * @property string streetAddress
 */
class Address extends BaseModel
{
    /** @var string */
    public $countryCode;
    /** @var string */
    public $postalCode;
    /** @var string */
    public $addressLocality;
    /** @var string */
    public $streetAddress;
}
