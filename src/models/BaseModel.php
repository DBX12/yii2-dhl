<?php


namespace dbx12\dhl_component\models;


use yii\base\Model;

class BaseModel extends Model
{
    protected $classMap = [];

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->createObjects($config);
    }

    protected function createObjects($config)
    {
        foreach ($this->classMap as $propertyName => $class) {
            if(!array_key_exists($propertyName,$config)){
                // $propertyName is not defined in $config
                continue;
            }
            $propertyConfig = $config[$propertyName];
            if (is_array($class)) {
                // it is an array, so iterate over $propertyConfig
                $class = $class[0];
                // clear the property because it contains the plain array from Yii::configure()
                $this->$propertyName = [];
                foreach ($propertyConfig as $singlePropertyConfig) {
                    $this->$propertyName[] = new $class($singlePropertyConfig);
                }
            } else {
                $this->$propertyName = new $class($propertyConfig);
            }
        }
    }
}
