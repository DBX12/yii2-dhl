<?php


namespace dbx12\dhl_component\models;

/**
 * Class Shipment
 *
 * @package dbx12\dhl_component\models
 * @property string id
 * @property string service
 * @property Place origin
 * @property Place destination
 * @property ShipmentEvent status
 * @property string estimatedTimeOfDelivery
 * @property array estimatedDeliveryTimeFrame
 * @property mixed estimatedTimeOfDeliveryRemark
 * @property string serviceUrl
 * @property string rerouteUrl
 * @property ShipmentDetails details
 * @property ShipmentEvent[] events
 */
class Shipment extends BaseModel
{
    /** @var string */
    public $id;
    /** @var string */
    public $service;
    /** @var Place */
    public $origin;
    /** @var Place */
    public $destination;
    /** @var ShipmentEvent */
    public $status;
    /** @var string */
    public $estimatedTimeOfDelivery;
    /** @var array */
    public $estimatedDeliveryTimeFrame;
    /** @var mixed */
    public $estimatedTimeOfDeliveryRemark;
    /** @var string */
    public $serviceUrl;
    /** @var string */
    public $rerouteUrl;
    /** @var ShipmentDetails */
    public $details;
    /** @var ShipmentEvent[] */
    public $events;

    protected $classMap = [
        'origin'      => Place::class,
        'destination' => Place::class,
        'status'      => ShipmentEvent::class,
        'details'     => ShipmentDetails::class,
        'events'      => [ShipmentEvent::class],
    ];
}
