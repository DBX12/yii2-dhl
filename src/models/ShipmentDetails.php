<?php


namespace dbx12\dhl_component\models;

/**
 * Class ShipmentDetails
 *
 * @package dbx12\dhl_component\models
 * @property Organization carrier
 * @property Product product
 * @property Person receiver
 * @property Person sender
 * @property ProofOfDelivery proofOfDelivery
 * @property int totalNumberOfPieces
 * @property string[] pieceIds
 * @property UnitValue weight
 * @property mixed volume
 * @property double loadingMeters
 * @property Dimensions dimensions
 * @property array references
 */
class ShipmentDetails extends BaseModel
{
    /** @var Organization */
    public $carrier;
    /** @var Product */
    public $product;
    /** @var Person */
    public $receiver;
    /** @var Person */
    public $sender;
    /** @var ProofOfDelivery */
    public $proofOfDelivery;
    /** @var int */
    public $totalNumberOfPieces;
    /** @var string[] */
    public $pieceIds;
    /** @var UnitValue */
    public $weight;
    /** @var UnitValue */
    public $volume;
    /** @var double */
    public $loadingMeters;
    /** @var Dimensions */
    public $dimensions;
    /** @var array */
    public $references;

    protected $classMap = [
        'carrier'    => Organization::class,
        'product'    => Product::class,
        'receiver' => Person::class,
        'sender' => Person::class,
        'proofOfDelivery' => ProofOfDelivery::class,
        'dimensions' => Dimensions::class,
        'weight' => UnitValue::class,
        'volume' => UnitValue::class,
    ];
}
