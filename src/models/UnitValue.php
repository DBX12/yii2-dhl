<?php


namespace dbx12\dhl_component\models;

/**
 * Class UnitValue
 *
 * @package dbx12\dhl_component\models
 * @property double value
 * @property string unitText
 */
class UnitValue extends BaseModel
{
    /** @var double */
    public $value;
    /** @var string */
    public $unitText;
}
