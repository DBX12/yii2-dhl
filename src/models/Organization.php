<?php


namespace dbx12\dhl_component\models;

/**
 * Class Organization
 *
 * @package dbx12\dhl_component\models
 * @property string organizationName
 */
class Organization extends BaseModel
{
    /** @var string */
    public $organizationName;
}
