<?php


namespace dbx12\dhl_component\models;

/**
 * Class Product
 *
 * @package dbx12\dhl_component\models
 * @property string productName
 */
class Product extends BaseModel
{
    /** @var string */
    public $productName;
}
