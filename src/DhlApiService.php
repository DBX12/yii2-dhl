<?php


namespace dbx12\dhl_component;


use dbx12\dhl_component\models\Shipment;
use dbx12\dhl_component\models\ShipmentResponse;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use yii\base\Exception;

class DhlApiService
{
    /** @var string */
    protected $baseUri;

    /** @var string */
    protected $consumerKey;

    /** @var HandlerStack */
    public $handlerStack;

    public function __construct($baseUri, $consumerKey)
    {
        $this->baseUri = $baseUri;
        $this->consumerKey = $consumerKey;
    }

    protected function getClient(): Client
    {
        $config = [
            'base_uri'                  => $this->baseUri,
            RequestOptions::HEADERS     => [
                'DHL-API-Key' => $this->consumerKey,
                'Accept'      => 'application/json',
            ],
            RequestOptions::HTTP_ERRORS => false,
        ];
        if ($this->handlerStack !== null) {
            $config['handler'] = $this->handlerStack;
        }
        return new Client($config);
    }

    /**
     * Searches a shipment by tracking number
     *
     * @param $trackingNumber
     * @return Shipment|null null if parcel was not found
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws Exception if the DHL API responds with a non-200 status code
     */
    public function getShipment($trackingNumber): ?Shipment
    {
        $client = $this->getClient();
        $response = $client->get('shipments', ['query' => ['trackingNumber' => $trackingNumber]]);
        switch ($response->getStatusCode()) {
            case 200:
                $shipmentResponse = new ShipmentResponse(
                    json_decode(
                        $response->getBody()->getContents(),
                        true
                    )
                );
                return $shipmentResponse->shipments[0] ?? null;
            case 404:
                return null;
            case 429:
                throw new Exception('DHL Api returned 429 quota exceeded', 429);
            default:
                throw new Exception('DHL Api replied with non-200 status', $response->getStatusCode());
        }
    }
}
